OSM property editor for umbraco
=================================

An Umbraco v7 OpenStreetMaps

Installation
===

1. Install the [NuGet package](https://www.nuget.org/packages/Umbraco.OpenStreetMap-propertyeditor/)

-or-

1. Build the solution, drop the assembly in the bin folder of your Umbraco v7 installation
2. Drop the App_Plugins folder from this repo into the corresponding folders of your Umbraco v7 installation

-then-

1. Create a new datatype of type "OSM Picker" and add a property of this new datatype in any of your document types.
2. click the map to place a marker

Bugs, feedback or suggestions
===

Please add any bugs or feature request to the issue tracker. Or give me shout at [@dampeebe](https://www.twitter.com/dampeebe).

Enjoy

__ This packages was based on the [Google Maps package](https://our.umbraco.org/projects/backoffice-extensions/google-maps-property-editor-w-google-places-autocomplete-lookup/)
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Our.Umbraco.OsmMaps.Models
{
    public struct OsmMapsLocation
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public int ZoomLevel { get; set; }
    }
}